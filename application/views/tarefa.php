		<section style="min-height: calc(100vh - 83px)" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-3 col-lg-6 text-center">
						<div class="section-title">
							<h2>Tarefas</h2>
								
						</div>
					</div>
				</div>
			</div>
<div class="container">
	<ul class="nav nav-tabs">
		<li  class="active"><a href="#tab_todas" role="tab" data-toggle="tab" id="link_todas">Pendente</a></li>
		<li><a href="#tab_fazendo" role="tab" data-toggle="tab">Executando</a></li>
		<li><a href="#tab_concluido" role="tab" data-toggle="tab">Concluído</a></li>

	</ul>

	<div class="tab-content">
		<div id="tab_todas" class="tab-pane active">
			<div class="container-fluid">
				<h2>Todas as tarefas</h2>
				
				<a id="btn_add_tarefa" class="btn btn-primary btn-lg"><i class="fa fa-plus">&nbsp;Criar Tarefa</i></a>
				<table id="dt_tarefas" class="table table-hover">
					<thead>
						<tr class="tableheader">
							<th class="no-sort">Data</th>
							<th class="no-sort">Descrição</th>
							<th class="no-sort">Documento</th>							
							<th class="no-sort">Ações</th>	
						</tr>
						
					</thead>
				</table>
			</div>
		</div>
		<div id="tab_fazendo" class="tab-pane">
			<div class="container-fluid">
			<h2>Minhas Tarefas</h2>
		
		
				<table id="dt_minhas_tarefas" class="table table-hover">
					<thead>
						<tr class="tableheader">
							<th class="no-sort">Data</th>
							<th class="no-sort">Descrição</th>
							<th class="no-sort">Documento</th>							
							<th class="no-sort">Ações</th>	
						</tr>
						
						
					</thead>
				</table>	
			
			</div>
		</div>
		<div id="tab_concluido" class="tab-pane">
			<div class="container-fluid">
			<h2>Tarefas concluídas</h2>
		
		
				<table id="dt_tarefas_concluidas" class="table table-hover">
					<thead>
						<tr class="tableheader">
							<th class="no-sort">Data</th>
							<th class="no-sort">Descrição</th>
							<th class="no-sort">Documento</th>							
							<th class="no-sort">Ações</th>	
						</tr>
						
						
					</thead>
				</table>	
			
			</div>
		</div>
</div>
</div>
		</section>
		
		
		<div id="modal_detalhes" class="modal fade">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">x</button>
						<h4 class="modal-title">Detalhes da tarefa</h4>
					</div>
					<div class="modal-body" id="list-tarefa">
	
					</div>
				</div>
			</div>			
		</div>
		<div id="modal-tarefa" class="modal fade">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">x</button>
						<h4 class="modal-title">Detalhes da Tarefa</h4>
					</div>
					<div id="loader"></div>
					<div class="modal-body" id="form-tarefa">
	
	

	
	
					</div>
				</div>
			</div>			
		</div>