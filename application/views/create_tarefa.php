<p />
<div class="container-fluid">
	<h3>Criar Tarefa</h3>
	<div>
		<form id="form_nova_tarefa" method="post" action="#">
			<input name="protocolo_idprotocolo" hidden="hidden"
				value="<?php echo $idprotocolo;?>" />
			<div class="form-group">
				<label class="col-lg-2 control-label">Descrição</label>
				<div class="col-lg-10">
					<textarea id="tf_descricao" name="tf_descricao"
						class="form-control"></textarea>
					<span class="help-block"></span>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-2 control-label">Tipo</label>
				<div class="col-lg-10">
					<select id="tf_fluxo" name="tf_fluxo" class="form-control">
						<option value="">Selecione...</option>
						<option value="1">Receber / Encaminhar</option>
						<option value="2">Receber / Dar Ciência / Arquivar</option>
						<option value="3">Licitações - Modalidade: Pregão</option>
					</select> <span class="help-block"></span>
				</div>
			</div>
<?php if($this->session->userdata('user_tipo') == 'Gestor'){?>
			<div class="form-group">
				<label class="col-lg-2 control-label">Responsável</label>
				<div class="col-lg-10">

					<select id="tf_usuario" name="tf_usuario" class="form-control">
                               <option value="0">Todos podem assumir</option>

                        <?php 
                            foreach($responsaveis as $resp){?>
                               <option value="<?php echo $resp->idusuario?>"><?php echo $resp->u_nome?></option>
                    
                        <?php }?>
            </select> <span class="help-block"></span>

				</div>

			</div>
<?php };?>
			<div class="form-group text-center">
				<button type="submit" id="btn_save_tarefa" class="btn btn-primary">
					<i class="fa fa-save">&nbsp;Criar Tarefa</i>
				</button>
				<span class="help-block"></span>
			</div>

		</form>
	</div>
</div>

<script>
    

$('#form_nova_tarefa').submit(function(){
       
        $.ajax({
            type:'POST',
            url: BASE_URL+'tarefa/ajax_criar_tarefa',
            dataType:'json',
            data:$(this).serialize(),
            beforeSend:function(){

            $('#loader').html(loader);
            $('#modal-tarefa').modal();
            	
            },
    success:function(response){
       // clearErrors();
 
        if(response['status']){
           $('#loader').html('');          
           $('#modal-tarefa').modal('hide');
            alert(response['message']);

            
        }else{
            $('#loader').html('');             
            showErrorsModal(response['error_list']);
        }
    },
        error: function(response){
             console.log(response);
            }
            
        });
        
        return false;
    });


  
</script>
