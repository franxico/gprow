<p />
<div class="container-fluid">
<h4>Nº do protocolo:<?php echo $tramitacao[0]->p_numero?></h4>
<div class="alert alert-warning">
<p>Descrição: <strong><?php echo $tramitacao[0]->p_descricao;?></strong></p>
</div>

<p><strong>Arquivos:</strong><br />
<?php

if($tramitacao[0]->nome){
?>

<a  class="bnt btn btn-primary" href="<?php echo base_url('public/images/'.$tramitacao[0]->nome);?>" target="_blank">
	<i class="fa fa-file-pdf-o fa-2x">&nbsp;Visualizar</i></a>
<?php     
    
}else{
    echo 'Nenhum arquivo enviado';
    
}

?>
</strong></p>

<table id="dt_protocolos" class="table table-hover ">
	<thead>
		<tr class="tableheader">
			<th>Data</th>
			<th class="no-sort">Descrição</th>
			<th>Local</th>
			<th>Tipo</th>
		</tr>

	</thead>
<h5>Movimentação</h5>
<?php
foreach ($tramitacao as $tramite) {

    ?>
<tr>

		<td><?php echo date_format(date_create($tramite->t_data),'d/m/y H:s');?></td>
		<td><?php echo $tramite->t_descricao;?></td>
		<td><?php echo $tramite->t_local;?></td>
		<td><?php echo $tramite->t_tipo;?></td>
	</tr>    
<?php
}
?>

					</table>
					</div>
