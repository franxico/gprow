<p />
<div class="container-fluid">
<h3>Criar Tarefa</h3>
<div>
	<form id="form_nova_tarefa" method="post"
		action="#">
		<input name="protocolo_idprotocolo" hidden value="<?php echo $idprotocolo;?>"/>
		<div class="form-group">
			<label class="col-lg-2 control-label">Descrição</label>
			<div class="col-lg-10">
				<textarea id="tf_descricao" name="tf_descricao" class="form-control"></textarea>
				<span class="help-block"></span>
			</div>
		</div>


<div class="form-group">
	<label class="col-lg-2 control-label">Tipo</label>
	<div class="col-lg-10">
		<select id="tf_fluxo" name="tf_fluxo" class="form-control">
			<option value="">Selecione...</option>
			<option value="1">Receber/Encaminha</option>
			<option value="2">Receber/Dar Ciência</option>
		</select> <span class="help-block"></span>
	</div>
</div>
<div class="form-group text-center">
	<button type="submit" id="btn_save_tarefa" class="btn btn-primary">
		<i class="fa fa-save">&nbsp;Criar</i>
	</button>
	<span class="help-block"></span>
</div>

</form>
</div>
</div>
