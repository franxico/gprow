<?php

/**
 * 
 */
class Protocolo_model extends CI_Model


{
    
    private $column_search = array('p_descricao','idprotocolo','p_origem');
    private $column_order = array('p_data',' ','p_origem');
    
	
	public function __construct()
	{
    	parent::__construct();
    	$this->load->database();
	}


	public function get_data($id, $select = NULL){
	    
	    if(!empty($select)){
	        
	        $this->db->select($select);
	    }

	    $this->db->from('protocolo');
	    $this->db->where('idprotocolo', $id);
	    

	    
	    return $this->db->get()->result();
	}
	

	public function get_tramitacao($id, $select = NULL){
	    
	    if(!empty($select)){
	        
	        $this->db->select($select);
	    }
	    
	    $this->db->from('tramitacao t');
	    $this->db->join('protocolo p','t.protocolo_idprotocolo = p.idprotocolo','left');
	    $this->db->join('arquivo a','t.protocolo_idprotocolo = a.protocolo_idprotocolo','left');
	    $this->db->where('t.protocolo_idprotocolo', $id);
	    
	    
	    return $this->db->get()->result();
	}
	

	
	public function insert($data){
	    $this->db->insert('protocolo', $data);
	    
	    return $this->db->insert_id();
	}
	
	public function insert_arquivo($data){
	    $this->db->insert('arquivo', $data);
	    
	    return $this->db->insert_id();
	}
	
	public function insert_tramitacao($data){
	    $this->db->insert('tramitacao',$data);
	    
	    return $this->db->insert_id();
	    
	    
	}
	
	public function update($id, $data){
	    $this->db->where('idprotocolo', $id);
	    $this->db->update('protocolo', $data);
	}
	
	public function delete($id){
	    $this->db->where('idprotocolo', $id);
	    $this->db->delete('protocolo');
	}
	
	public function is_duplicated($field, $value, $id=NULL){
	    if(!empty($id)){
	        $this->db->where('idprotocolo <>', $id);
	    }
	    
	    $this->db->from('protocolo');
	    $this->db->where($field, $value);
	    
	    return $this->db->get()->num_rows() > 0;

	}
	
	
    public function exist($fields)
    {
		//Verifica se existe algum registro com os valores informados
        $this->db->from('protocolo');
        $this->db->where($fields);

        return $this->db->get()->num_rows() > 0;
    }
	
	private function _get_dataTable(){

	    
	    $search = NULL;
	    
	    if($this->input->post('search')){
	        $search = $this->input->post('search')['value'];
	    }
	    
	    
	    $order_column = NULL;
	    $order_dir = NULL;
	    $order = $this->input->post('order');
	    
	    if(isset($order)){
	        $order_column = $order[0]['column'];
	        $order_dir = 'DESC';
	    }
	    
	    $this->db->from('protocolo');
	    //$this->db->join('tarefa', 'protocolo.idprotocolo = tarefa.protocolo_idprotocolo');
	    
	 //   echo $this->db->last_query();
	    
	    if(isset($search)){
	        $first  = TRUE;
	        
	        foreach ($this->column_search as $field) {
	            if($first){
	                $this->db->group_start();
	                $this->db->like($field,$search);
	                $first = FALSE;
	            }else{
	                $this->db->or_like($field,$search);
	            }
	        }
	        
	        if(!$first){
	            $this->db->group_end();
	        }
	    }
	    
	    if(isset($order)){
	        $this->db->order_by($this->column_order[$order_column].' '.$order_dir);
	    }else{
	        $this->db->order_by('p_data desc');
	    }
	}
	
	public function get_dataTable(){
	    $length = $this->input->post('length');
	    $start = $this->input->post('start');
	    
	    $this->_get_dataTable();
	    
	    if(isset($length) && $length != -1){
	        $this->db->limit($length, $start);
	    }
	    
	    return $this->db->get()->result();
	    
	}
	
	public function records_filtered(){
	    $this->_get_dataTable();
	    return $this->db->get()->num_rows();
	    
	}
	
	public function records_total(){
	    $this->db->from('protocolo');
	   // $this->db->join('tarefa', 'protocolo.idprotocolo = tarefa.protocolo_idprotocolo');
	    
	    return $this->db->count_all_results();
	}
	

}