		<section style="min-height: calc(100vh - 83px)" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-3 col-lg-6 text-center">
						<div class="section-title">
							<h2 style="margin-top:100px;">Bem vindo ao GProw</h2>
							<p>
								Seu acesso ao GPROw já está disponível no link <a href="https://paradigmasistemas.com/gprow/restrict">https://paradigmasistemas.com/gprow/restrict</a>  
							</p>
							<h3>Antes de realizar seu primeiro acesso, algumas informações importantes</h3>

							
<p> Nesta versão inicial contamos com você para evoluir nosso sistema. <br />
<br />Por essa razão disponibilizamos um canal para informar ocorrências ou sugestões.<a href="https://bitbucket.org/franxico/gprow/issues/new"> Para acessar, basta clicar aqui.</a></p>
						
<p>Aliás, em caso de dúvida você pode consultar <a href="https://paradigmasistemas.com/roteiro_gprow.pdf">esse guia rápido do sistema</a></p>

<p>Pronto, você já pode acessar o sistema: seu usuário é o email cadastrado.</p>
<p>
    <a href="https://paradigmasistemas.com/gprow/restrict">https://paradigmasistemas.com/gprow/restrict</a> 
    
</p>						
</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</section>