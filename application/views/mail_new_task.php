		
		<section style="min-height: calc(100vh - 83px)" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-3 col-lg-6 text-center">
						<div class="section-title">
							<h2 style="margin-top:100px;">Uma nova atividade foi atribuída  a você</h2>
							<p>
								Para visualizá-la acesse <a href="https://paradigmasistemas.com/gprow/restrict">https://paradigmasistemas.com/gprow/restrict</a>  
							</p>
							<h3>Detalhes da Tarefa</h3>

							<h4><?php echo $mensagem['descricao']?></h4>

</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</section>