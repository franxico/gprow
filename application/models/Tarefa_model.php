<?php

/**
 * 
 */
class Tarefa_model extends CI_Model

{

    private $column_search = array(
        'tf_descricao',
        'protocolo_idprotocolo',
        'tf_data'
    );

    private $column_order = array(
        'tf_data'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_data($id, $select = NULL)
    {
        if (! empty($select)) {

            $this->db->select($select);
        }

        $this->db->from('tarefa');
        $this->db->where('idtarefa', $id);

        return $this->db->get()->result()[0];
    }

    public function get_data_by_status($select = NULL)
    {
        if (! empty($select)) {

            $this->db->select($select);
        }

        $this->db->from('tarefa');
        $this->db->order_by('tf_data');

        return $this->db->get()->result();
    }

    public function list_atividades($id, $select = NULL)
    {
        if (! empty($select)) {

            $this->db->select($select);
        }

        $this->db->from('atividade');
        $this->db->where('tarefa_idtarefa', $id);
        $this->db->order_by('idatividade Desc');

        return $this->db->get()->result();
    }

    public function insert_atividade($dados)
    {
        $this->db->insert('atividade', $dados);

        return $this->db->insert_id();
    }

    public function insert($data)
    {
        $this->db->insert('tarefa', $data);

        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where('idtarefa', $id);
        $this->db->update('tarefa', $data);

        return $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->where('idprotocolo', $id);
        $this->db->delete('protocolo');
    }

    public function is_duplicated($field, $value, $id = NULL)
    {
        if (! empty($id)) {
            $this->db->where('idprotocolo <>', $id);
        }

        $this->db->from('protocolo');
        $this->db->where($field, $value);

        return $this->db->get()->num_rows() > 0;
    }

    private function _get_dataTable()
    {
        $search = NULL;

        if ($this->input->post('search')) {
            $search = $this->input->post('search')['value'];
        }

        $order_column = NULL;
        $order_dir = NULL;
        $order = $this->input->post('order');

        if (isset($order)) {
            $order_column = $order[0]['column'];
            $order_dir = 'DESC';
        }

        $this->db->from('tarefa t');
        $this->db->join('protocolo p', 't.protocolo_idprotocolo = p.idprotocolo');

        if (! empty($this->input->post('usuario'))) {
            $usuario = $this->input->post('usuario');

            $this->db->where('tf_usuario', $usuario);
        }
        if (! empty($this->input->post('status'))) {
            $status = $this->input->post('status');

            $this->db->where('tf_situacao', $status);
        }



        if (isset($search)) {
            $first = TRUE;

            foreach ($this->column_search as $field) {
                if ($first) {
                    $this->db->group_start();
                    $this->db->like($field, $search);
                    $first = FALSE;
                } else {
                    $this->db->or_like($field, $search);
                }
            }

            if (! $first) {
                $this->db->group_end();
            }
        }

        if (isset($order)) {
            $this->db->order_by($this->column_order[$order_column] . ' ' . $order_dir);
        } else {
            $this->db->order_by('tf_data desc');
        }
    }

    public function get_dataTable()
    {
        $length = $this->input->post('length');
        $start = $this->input->post('start');

        $this->_get_dataTable();

        if (isset($length) && $length != - 1) {
            $this->db->limit($length, $start);
        }

        return $this->db->get()->result();
    }

    public function records_filtered()
    {
        $this->_get_dataTable();
        return $this->db->get()->num_rows();
    }

    public function records_total()
    {
        $this->db->from('tarefa');
        return $this->db->count_all_results();
    }
}