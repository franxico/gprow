
<section style="min-height: calc(100vh - 83px)" class="light-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6 text-center">
				<div class="section-title">
					<h2>Área Restrita</h2>

				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_recebidos" role="tab"
				data-toggle="tab">Documentos Recebidos</a></li>
<!-- 			<li><a href="#tab_emitidos" role="tab" data-toggle="tab">Documentos
					Emitidos</a></li> -->
		</ul>

		<div class="tab-content">
			<div id="tab_recebidos" class="tab-pane active">
				<div class="container-fluid">
					<h2>Documentos recebidos</h2>

					<a id="btn_add_protocolo" class="btn btn-primary btn-lg"><i
						class="fa fa-plus">&nbsp;Receber documento</i></a>
					<table id="dt_protocolos" class="table table-hover">
						<thead>
							<tr class="tableheader">
								<th class="no-sort">Data</th>
								<th class="no-sort">Documento</th>
								<th class="no-sort">Descrição</th>
								<th class="no-sort">Ações</th>
							</tr>

						</thead>
					</table>
				</div>
			</div>
<!-- 			<div id="tab_emitidos" class="tab-pane">Documentos Emitidos</div>
 -->		</div>
	</div>
</section>

<div id="modal_recebidos" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Receber Documentos</h4>
			</div>
			<div id="loader"></div>			
			<div class="modal-body">
				<form id="form_recebimento" method="post" action="" enctype="multipart/form-data">
					<input name="idprotocolo" hidden="hidden" />

					<div class="form-group">
						<label class="col-lg-2 control-label">Origem</label>
						<div class="col-lg-10">
							<input id="p_origem" name="p_origem" class="form-control" /> <span
								class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Destino</label>
						<div class="col-lg-10">
							<input id="p_destino" name="p_destino" class="form-control" /> <span
								class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Descrição</label>
						<div class="col-lg-10">
							<textarea id="p_descricao" name="p_descricao"
								class="form-control"></textarea>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Nº Documento</label>
						<div class="col-lg-10">
							<input id="p_num_origem" name="p_num_origem" class="form-control"
								placeholder="Ex.: Memorando 010/2020" /> <span
								class="help-block"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-2 control-label">Tipo</label>
						<div class="col-lg-10">
							<select id="p_tipo" name="p_tipo" class="form-control">
								<option value="">Selecione...</option>
								<option value="Solicitações em Geral">Solicitações em Geral</option>
								<option value="Memorandos Informativo">Memorandos Informativos</option>
								<option value="Solicitação de Diária">Solicitação de Diária</option>
								<option value="Solicitação de Compra">Solicitação de Compra</option>
								<option value="Recebimento de Notas Fiscais">Recebimento de Notas Fiscais</option>
								<option value="Relatório de Diárias">Relatório de Diárias</option>
							</select> <span class="help-block"></span>
						</div>

					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Anexar Arquivo</label>
						<div class="col-lg-10">
							<input type="file" id="userfile" name="image_file" class="form-control"
								/> <span
								class="help-block"></span>
							<input type="text" id="file_uploaded" name="file_uploaded" class="file_uploaded" />
						</div>
					</div>					
					<div class="form-group">
					
						<div class="col-lg-6">
							<label class="control-label" for="p_despacho">Documento necessita Despacho 
							<input type="checkbox" class="form-check-input" name="p_despacho" id="p_despacho" checked value="1"></label>

						</div>
					</div>
					<div class="form-group">
						<button type="submit" id="btn_save_doc" class="btn btn-success">
							<i class="fa fa-save">&nbsp;Salvar</i>
						</button>
						<span class="help-block"></span>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
<div id="modal_detalhes_protocolo" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h3 class="modal-title">Detalhes do Protocolo</h3>
			</div>
			<div class="modal-body" id="list-tramitacao">
				<div class="loader" role="status" style="margin:50px auto;">
  					<span class="sr-only">Loading...</span>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="modal-tarefa" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Detalhes do Protocolo</h4>
			</div>
			<div class="modal-body" id="form-tarefa">
				<div class="loader" role="status" style="margin:50px auto;">
  					<span class="sr-only">Loading...</span>
				</div>				</div>
		</div>
	</div>
</div>
