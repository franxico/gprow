		<section style="min-height: calc(100vh - 83px)" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-3 col-lg-6 text-center">
						<div class="section-title">
							<h2 style="margin-top:100px;">Novo Usuário</h2>
								<form id="new_user_form" method="post">
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-user"></span>
													</div>
													<input type="text" placeholder="Nome e Sobrenome" name="u_name"  id="u_name" class="form-control input-lg">
												</div>
												<span class="help-block"></span>

											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-envelope"></span>
													</div>
													<input type="text" placeholder="Email. Obs.: preferencial gmail" name="u_email"  id="u_email" class="form-control input-lg">
												</div>
												<span class="help-block"></span>

											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-lock"></span>
													</div>
													<input type="password" placeholder="Senha" name="u_password" id="u_password" class="form-control input-lg">
												</div>
													<span class="help-block"></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-lock"></span>
													</div>
													<input type="password" placeholder="Repita a Senha" name="u_repassword" id="u_repassword" class="form-control input-lg">
												</div>
												<span class="help-block"></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<button type="submit" id="btn_new_user" class="btn btn-lg btn-block btn-primary">Cadastrar Usuário</button>
											</div>
											<span class="help-block"></span>
										</div>
									</div>																			
								</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</section>