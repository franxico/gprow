$(function(){
    $('#btn_add_tarefa').click(function(){

          id = '#modal-tarefa';                
                 
            $('#modal-tarefa').modal();          
              
              id_protocolo = $(this).attr('protocolo_id');                
                $.ajax({
                    type:'POST',
                    url: BASE_URL+'tarefa/ajax_nova_tarefa/1',
                    dataType:'json',
                    data:$(this).serialize(),
                    beforeSend:function(){

                    $('#loader').html(loader);
                    $('#modal-tarefa').modal();

                    },
            success:function(response){
                //clearErrors();
         
                if(response['status']){
                   $('#loader').html('');
                   $('#form-tarefa').html(response['data']);                    


                    
                }else{
                   // $('#loader').html('');                    
                    showErrorsModal(response['error_list']);
                }
            },
                error: function(response){
                     console.log(response);
                    }
                    
                });    

    });
    
    
    function active_btn_protocolo(){

           $('.btn-view-detalhes').click(function(){
              
               id_tarefa = $(this).attr('tarefa_id');                
                $.ajax({
                    type:'POST',
                    url: BASE_URL+'tarefa/ajax_get_detalhes_tarefa/'+id_tarefa,
                    dataType:'json',
                    data:$(this).serialize(),
                    beforeSend:function(){
                    clearErrors();

                    $('#list-tarefa').html(loader);
                    $('#modal_detalhes').modal();

                    },
            success:function(response){
                //clearErrors();
         
                if(response['status']){

                    $("#list-tarefa").html(response['data']);                    
                    $('#modal_detalhes').modal();

                    
                }else{
                    showErrorsModal(response['error_list']);
                }
            },
                error: function(response){
                     console.log(response);
                    }
                    
                });                
                    

            
            });  
                
        
    }
    
    
    var dt_tarefas_concluidas = $('#dt_tarefas_concluidas').dataTable({
        'lengthChange': false,
        'autoWidth':false,
        'processing':true,
        'serverSide':true,
        'ajax': {
            'url':BASE_URL+'tarefa/ajax_list_tarefas/concluida/usuario',
            'type': 'POST'
        },
        "language": {
            "url": "public/js/Portuguese-Brasil.json"},        
        'columnDefs':[
            {targets: 'no-sort', orderable:false},
            {target:'dt-center', className:'dt-center'},
            {targets: 0, "width": '5%'},
            {targets: 1, "width": '60%'},            
            {targets: 2, "width": '20%'},               
            {targets: 3, "width": '15%'}               
        ],
        'initComplete':function(){
            //window.alert('kl');
            active_btn_protocolo();
        },
        'drawCallback':function(){
            //window.alert('kl');
            active_btn_protocolo();
        }
    });
    

    var dt_minhas_tarefa = $('#dt_minhas_tarefas').dataTable({
        'lengthChange': false,
        'autoWidth':false,
        'processing':true,
        'serverSide':true,
        'ajax': {
            'url':BASE_URL+'tarefa/ajax_list_tarefas/iniciada/usuario',
            'type': 'POST'
        },
        "language": {
            "url": "public/js/Portuguese-Brasil.json"},        
        'columnDefs':[
            {targets: 'no-sort', orderable:false},
            {target:'dt-center', className:'dt-center'},
            {targets: 0, "width": '5%'},
            {targets: 1, "width": '60%'},            
            {targets: 2, "width": '20%'},               
            {targets: 3, "width": '15%'}               
        ],
        'initComplete':function(){
            //window.alert('kl');
            active_btn_protocolo();
        },
        'drawCallback':function(){
            //window.alert('kl');
            active_btn_protocolo();
        }
    });    
    
    
    //function load_table(){
    var dt_todas_tarefas = $('#dt_tarefas').dataTable({
        'lengthChange': false,
        'autoWidth':false,
        'processing':true,
        'serverSide':true,
        'ajax': {
            'url':BASE_URL+'tarefa/ajax_list_tarefas/criada',
            'type': 'POST'
        },
        "language": {
            "url": "public/js/Portuguese-Brasil.json"},        
        'columnDefs':[
            {targets: 'no-sort', orderable:false},
            {target:'dt-center', className:'dt-center'},
            {targets: 0, "width": '5%'},
            {targets: 1, "width": '60%'},            
            {targets: 2, "width": '20%'},               
            {targets: 3, "width": '15%'}            
        ],
        'initComplete':function(){
            //window.alert('kl');
            active_btn_protocolo();
        },
        'drawCallback':function(){
            //window.alert('kl');
            active_btn_protocolo();
        }
    });    
    


    
   
   $('#modal_detalhes').on('hide.bs.modal', function(){
       //alert('fechou');
       dt_minhas_tarefa.api().ajax.reload();
       dt_todas_tarefas.api().ajax.reload();
       dt_tarefas_concluidas.api().ajax.reload();
   });
   
   $('#modal-tarefa').on('hide.bs.modal', function(){
       //alert('fechou');
       dt_minhas_tarefa.api().ajax.reload();
       dt_todas_tarefas.api().ajax.reload();
       dt_tarefas_concluidas.api().ajax.reload();
       
   });
   



})