<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Restrict extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        if ($this->session->userdata('user_id')) {
            $data = array(
                'styles' => array(
                    'dataTables.bootstrap.min.css',
                    'datatables.min.css'
                ),
                'scripts' => array(
                    'jquery.dataTables.min.js',
                    'dataTables.bootstrap.min.js',
                    'datatables.min.js',
                    'utils.js',
                    'restrict.js',
                    'tarefa.js'
                )
            );

            if ($this->session->user_tipo == 'Gestor') {
                $inicio = 'protocolo.php';
            } else {
                $inicio = 'tarefa.php';
            }

            $this->template->show($inicio, $data);
        } else {

            $data = array(
                'scripts' => array(
                    'utils.js',
                    'login.js'
                ),
                'btn_login' => 'Entrar'
            );
            $this->template->show('login', $data);
        }
    }

    function novo_usuario()
    {
        $data = array(
            'scripts' => array(
                'utils.js',
                'login.js'
            )
        );
        $this->template->show('create_usuario', $data);
    }

    function logoff()
    {
        $this->session->sess_destroy();
        header('Location:' . base_url('restrict'));
    }

    function ajax_create_user()
    {
        if (! $this->input->is_ajax_request()) {
            header('Location:' . base_url('restrict'));
        }

        $json = array();
        $json['status'] = 1;
        $json['error_list'] = array();

        $data = $this->input->post();

        $data['u_orgao'] = 'SMSI';

        if (empty($data['u_name'])) {
            $json['status'] = 0;
            $json['error_list']['#u_name'] = 'Campo nome está vazio';
        }

        if (empty($data['u_email'])) {
            $json['status'] = 0;
            $json['error_list']['#u_email'] = 'Campo email está vazio';
        }

        if (empty($data['u_password'])) {
            $json['status'] = 0;
            $json['error_list']['#u_password'] = 'Campo senha está vazio';
        }

        if (($data['u_password'] != $data['u_repassword'])) {
            $json['status'] = 0;
            $json['error_list']['#u_repassword'] = 'Os campos de senha devem ser iguais';
        }

        if ($json['status']) {

            $this->load->model('users_model');

            $campos = array(
                'u_email' => $data['u_email']
            );

            $is_duplicate = $this->users_model->is_duplicate($campos);
            // $result = $this->users_model->get_user_data($username);

            if (! $is_duplicate) {

                $user = array(
                    'u_nome' => $data['u_name'],
                    'u_email' => $data['u_email'],
                    'u_orgao' => $data['u_orgao'],
                    'u_usuario' => $data['u_email'],
                    'u_senha' => sha1($data['u_password']),
                    // @TODO criar prefixo e campo tipo
                    'u_prefixo' => 'PREFIX',
                    'u_tipo' => 'Departamento'
                );

                $result = $this->users_model->insert($user);

                if ($result) {
                    $json['status'] = 1;
                    $json['error_list']['#btn_new_user'] = '<div class="alert alert-success">Cadastro conclúido. Verifique seu email para ativar a conta.</div>';
                }

                $dados['mensagem'] = $user;

                $msg = $this->load->view('mail_new_user', $dados, true);
                $assunto = 'Confirmação de Cadastro no GPROw';
                $email = $user['u_email'];

                $this->_send_email($email, $msg, $assunto);
            } else {
                $json['status'] = 0;
            }

            if ($json['status'] == 0) {
                $json['error_list']['#btn_new_user'] = '<div class="alert alert-danger">Já existe um usuário cadastrado com esse email.</div>';
            }
        }

        echo json_encode($json);
    }

    private function _send_email($email, $msg, $assunto)
    {
        $this->load->library('email');

        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->from('gprow_smsi@paradigmasistemas.com', 'GPROw - Gerenciador de Tarefas');
        $this->email->to($email);

        $this->email->subject($assunto);
        $this->email->message($msg);

        $this->email->send();
    }

    function ajax_login()
    {
        if (! $this->input->is_ajax_request()) {
            header('Location:' . base_url('restrict'));
        }

        $json = array();
        $json['status'] = 1;
        $json['error_list'] = array();

        $username = $this->input->post('username');
        $passrword = $this->input->post('password');

        if (empty($username)) {
            $json['status'] = 0;
            $json['error_list']['#username'] = 'Digite um nome de usuário';
        } else {

            $this->load->model('users_model');
            $result = $this->users_model->get_user_data($username);

            if ($result) {
                $user_id = $result->idusuario;
                $user_password = $result->u_senha;

                if (sha1($passrword) == $user_password) {

                    $user = array(
                        'user_id' => $user_id,
                        'user_name' => $result->u_nome,
                        'user_email' => $result->u_email,
                        'user_orgao' => $result->u_orgao,
                        'user_prefixo' => $result->u_prefixo,
                        'user_tipo' => $result->u_tipo
                    );

                    $this->session->set_userdata($user);
                } else {
                    $json['status'] = 0;
                }
            } else {
                $json['status'] = 0;
            }

            if ($json['status'] == 0) {
                $json['error_list']['#btn_login'] = 'Usuário e/ou senha incorretos';
            }
        }

        echo json_encode($json);
    }
}
