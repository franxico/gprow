<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Protocolo extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');

        if (! $this->session->userdata('user_id')) {
            $data = array(
                'scripts' => array(
                    'utils.js',
                    'login.js'
                ),
                'btn_login' => 'Entrar'
            );
            $this->template->show('login', $data);
        }
    }

    public function index()
    {
        if ($this->session->userdata('user_id')) {
            $data = array(
                'styles' => array(
                    'dataTables.bootstrap.min.css',
                    'datatables.min.css'
                ),
                'scripts' => array(
                    'jquery.dataTables.min.js',
                    'dataTables.bootstrap.min.js',
                    'datatables.min.js',
                    'utils.js',
                    'restrict.js'
                )
            );

            $this->template->show('protocolo.php', $data);
        } else {

            $data = array(
                'scripts' => array(
                    'utils.js',
                    'login.js'
                ),
                'btn_login' => 'Entrar'
            );
            $this->template->show('login', $data);
        }
    }

    public function ajax_save_tramitacao()
    {}

    function ajax_save_protocolo()
    {
        if (! $this->input->is_ajax_request()) {
            exit('Acesso direto não permitido');
        }

        $json = array();
        $json['status'] = 1;
        $json['error_list'] = array();

        $data = $this->input->post();

        if (empty($data['p_origem'])) {
            $json['status'] = 0;
            $json['error_list']['#p_origem'] = 'Campo origem está vazio';
        }
        if (empty($data['p_destino'])) {
            $json['status'] = 0;
            $json['error_list']['#p_destino'] = 'Campo destino está vazio';
        }

        if (empty($data['p_descricao'])) {
            $json['status'] = 0;
            $json['error_list']['#p_descricao'] = 'Campo descrição está vazio';
        }

        if (empty($data['p_num_origem'])) {
            $json['status'] = 0;
            $json['error_list']['#p_num_origem'] = 'Campo nº documento está vazio';
        }

        if (empty($data['p_tipo'])) {
            $json['status'] = 0;
            $json['error_list']['#p_tipo'] = 'O campo tipo não foi selecionado';
        }
                       

        if ($json['status']) {

            $n_protocolo = $this->session->userdata('user_id');

            $n_protocolo .= date('ymdHis');

            $data['solicitacao_idsolicitacao'] = 1;
            $data['p_numero'] = $n_protocolo;
            $data['p_data'] = date('Y-m-d H:m:s');
            $data['p_status'] = 'Aberto';

            $this->load->model('protocolo_model');

            $campos['p_descricao'] = $data['p_descricao'];
            $campos['p_origem'] = $data['p_origem'];
            // $campos['p_destino'] = $data['p_destino'];
            $campos['p_num_origem'] = $data['p_num_origem'];
            
            $arquivo['nome'] = $data['file_uploaded'];
            
            unset($data['file_uploaded']);

            if (! $this->protocolo_model->exist($campos)) {

                if (! empty($data['protocolo_id'])) {
                    $this->protocolo_model->update($data);
                } else {
                    $id_protocolo = $this->protocolo_model->insert($data);

                    if ($id_protocolo) {

                        $tramite['t_descricao'] = 'Documento recebido';
                        $tramite['t_data'] = date('Y-m-d H:m:i');
                        $tramite['t_local'] = $this->session->userdata('user_orgao');
                        $tramite['t_responsavel'] = $this->session->userdata('user_id');
                        $tramite['t_tipo'] = 'Interno';
                        $tramite['protocolo_idprotocolo'] = $id_protocolo;

                        $this->protocolo_model->insert_tramitacao($tramite);
                        
                        if(!empty($arquivo['nome'])){
                            
                            $file_name = basename($arquivo["nome"]);
                            $old_path = getcwd() . "/public/images/" . $file_name.'.pdf';
                            $new_path = getcwd() . "/public/images/protocolos/" . $file_name.'.pdf';
                            
                            rename($old_path, $new_path);
                            
                            $arquivo['protocolo_idprotocolo'] = $id_protocolo;
                            $arquivo['tipo'] = 'protocolos';
                            $arquivo['nome'] = '/'.$arquivo['tipo'].'/' . $file_name.'.pdf';
                            
                            
                            $this->protocolo_model->insert_arquivo($arquivo);
                        
                        }
                        
                    }
                    ;
                }
            } else {
                $json['status'] = 0;
                $json['error_list']['#p_descricao'] = 'Um protocolo com essa descrição já foi inserido no sistema';
            }
        }
        echo json_encode($json);

    }

    function ajax_upload()
    {
        $file_name = date('dd-mm-y_h_m_s');;
       // $arquivo = $_FILES['aq_arquivo'];
        
        $config["upload_path"] = "./public/images";
        $config["file_name"] = $file_name.'.pdf';
        $config["allowed_types"] = "png|pdf";
        
        $config["overwrite"] = TRUE;
        
        $json = array();
        $json["status"] = 1;
        
        $this->load->library('upload',$config);
        
        if ($this->upload->do_upload('file')){
            $json["img_path"] = base_url() . "/public/images/" . $file_name;
        }else{
            $json["status"] = 0;
            $json["error"] = $this->upload->display_errors("","");
        }
        
        
        echo json_encode($json);
    }

    public function ajax_view_barcode($idprotocolo)
    {
        $this->load->library('barcode');
        $this->load->model('protocolo_model');

        $result = $this->protocolo_model->get_data($idprotocolo);

        $n_protocolo = $result[0]->p_numero;
        $data['codigo'] = $this->barcode->getCode($n_protocolo);
        // $data['atendente'] = $result[0]->p_atendente;
        $data['texto'] = $n_protocolo;
        $data['data'] = date_format(date_create($result[0]->p_data), 'd/m/y h:m');

        $this->load->view('printcode', $data);
    }

    public function ajax_list_protocolos()
    {
        if (! $this->input->is_ajax_request()) {
            exit('Acesso direto não permitido');
        }
        ;

        $this->load->model('protocolo_model');
        $protocolos = $this->protocolo_model->get_dataTable();

        $data = array();

        foreach ($protocolos as $protocolo) {

            $button = '<button class="btn btn-light btn-view-protodetalhes" 
                        protocolo_id="' . $protocolo->idprotocolo . '">
	                       <i class="fa fa-search-plus">&nbsp;Detalhes</i>
                        </button>';

            if ($this->session->userdata('user_tipo') != 'Gestor') {
                $button .= ' <button class="btn btn-light btn-view-barcode"
	                protocolo_id="' . $protocolo->idprotocolo . '">
	                <i class="fa fa-print">&nbsp;Etiqueta</i>
	                </button>';
            }
            ;

            if (($protocolo->p_status == 'Aberto')) {

                if ((($protocolo->p_despacho == 0) and ($this->session->user_tipo != 'Gestor')) or (($protocolo->p_despacho == 1) and ($this->session->user_tipo == 'Gestor'))) {

                    $button .= ' <button class="btn btn-success btn-create-tarefa"
                                protocolo_id="' . $protocolo->idprotocolo . '">
    	                       <i class="fa fa-edit">Criar Tarefa</i>
                               </button>';
                }
                ;
            }

            if ($protocolo->p_status != 'Permanente') {
                // Oculta os protocolos de atividades internas dos departamentos
                $row = array();
                $row[] = date_format(date_create($protocolo->p_data), 'd/m/y');
                $row[] = $protocolo->p_num_origem . ' - <br>' . $protocolo->p_origem;
                $row[] = $protocolo->p_descricao;
                $row[] = '<div style="display:inline-block">' . $button . '</div>';

                $data[] = $row;
            }
        }

        $json = array(
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->protocolo_model->records_total(),
            'recordsFiltered' => $this->protocolo_model->records_filtered(),
            'data' => $data
        );

        echo json_encode($json);
    }

    public function ajax_list_tramitacao($id)
    {
        if (! $this->input->is_ajax_request()) {
            exit('Acesso direto não permitido');
        }
        ;

        $this->load->model('protocolo_model');
        $tramitacao = $this->protocolo_model->get_tramitacao($id);
        
       // echo $this->db->last_query();

        $dados['tramitacao'] = $tramitacao;
        $dados['idprotocolo'] = $id;

        $json['data'] = $this->load->view('list_tramitacao', $dados, true);
        $json['status'] = 1;

        echo json_encode($json);
    }
}
