<?php

/**
 * 
 */
class Users_model extends CI_Model
{
	
	public function __construct()
	{
    	parent::__construct();
    	$this->load->database();
	}

	public function get_user_data($user_login){
		$this->db
			->select()
			->from('usuario')
			->where('u_usuario', $user_login);

		$result = $this->db->get();

		if($result->num_rows() > 0){
		    

			return $result->row();
		}else{
			return NULL;
		};
	}
	
	
	public function get_data($id, $select = NULL){
	    
	    if(!empty($select)){
	        
	        $this->db->select($select);
	    }

	    $this->db->from('usuario');
	    $this->db->where('idusuario', $id);
	    
	    return $this->db->get()->row();
	}

	public function get_users($data,$select = NULL){
	    
	    if(!empty($select)){
	        
	        $this->db->select($select);
	    }

	    $this->db->from('usuario');
	    $this->db->where($data);
		
		$result = $this->db->get();

		if($result->num_rows() > 0){
		    

			return $result->result();
		}else{
			return NULL;
		};

		
		

		
	}
	
	public function insert($data){
	    $result = $this->db->insert('usuario', $data);
	    
	    return $this->db->insert_id();
	}
	
	public function update($id, $data){
	    $this->db->where('idusuario', $id);
	    $this->db->update('usuarios', $data);
	}
	
	public function delete($id){
	    $this->db->where('idusuario', $id);
	    $this->db->delete('usuario');
	}
	
	public function is_duplicate($field, $id=NULL){
	    if(!empty($id)){
	        $this->db->where('idusuario <>', $id);
	    }
	    
	    $this->db->from('usuario');
	    $this->db->where($field);
	    
	    return $this->db->get()->num_rows() > 0;

	}
}