<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tarefa extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        
        if (!$this->session->userdata('user_id')) {
            $data = array(
                'scripts' => array(
                    'utils.js',
                    'login.js'
                ),
                'btn_login' => 'Entrar'
            );
            $this->template->show('login', $data);
        }
    }

    public function index()
    {
        if ($this->session->userdata('user_id')) {
            $data = array(
                'styles' => array(
                    'dataTables.bootstrap.min.css',
                    'datatables.min.css'
                ),
                'scripts' => array(
                    'jquery.dataTables.min.js',
                    'dataTables.bootstrap.min.js',
                    'datatables.min.js',
                    'utils.js',
                    'tarefa.js'
                )
            );

            $this->load->model('tarefa_model');

            // $dados['tf_situacao'] = ''
            $result = $this->tarefa_model->get_data_by_status();

            $lista = array();

            foreach ($result as $dados) {
                $lista[$dados->tf_situacao][] = $dados;
            }
            ;

            $data['tarefas'] = $lista;

            $this->template->show('tarefa.php', $data);
        } else {

            $data = array(
                'scripts' => array(
                    'utils.js',
                    'login.js'
                ),
                'btn_login' => 'Entrar'
            );
            $this->template->show('login', $data);
        }
    }

    public function ajax_nova_tarefa($idprotocolo)
    {
        $dados['idprotocolo'] = $idprotocolo;
        $json['status'] = 1;

        $this->load->model('users_model');

        $query_usuarios = array(
            'u_orgao' => 'smsi'
        );

        $dados['responsaveis'] = $this->users_model->get_users($query_usuarios, 'u_nome,idusuario');

        $json['data'] = $this->load->view('create_tarefa', $dados, true);

        echo json_encode($json);
    }

    public function avancar_tarefa($id_tarefa = 1)
    {
        $this->load->model('tarefa_model');

        $tarefa = $this->tarefa_model->get_data($id_tarefa);

        $fluxo = json_decode($tarefa->tf_fluxo);
        $etapa_atual = $fluxo->etapa_atual;
        $idprotocolo = $tarefa->protocolo_idprotocolo;

        switch ($tarefa->tf_situacao) {
            case 'Criada':
                $at_descricao = 'Atribuída';
                $tf_situacao = 'Iniciada';

                $json['reload'] = 1;

                break;
            case 'Iniciada':
                $at_descricao = 'Concluida';
                $tf_situacao = 'Iniciada';
                break;
        }
        ;

        $atividade = array(
            'at_etapa' => $etapa_atual,
            'at_situacao' => '...',
            'at_descricao' => $at_descricao,
            'at_data' => date('Y-m-d H:m:i'),
            'tarefa_idtarefa' => $id_tarefa,
            'at_responsavel' => $this->session->userdata('user_name')
        );

        $id_atividade = $this->tarefa_model->insert_atividade($atividade);

        if ($id_atividade) {

            $ultima_etapa = count($fluxo->etapas);

            if ($fluxo->etapa_atual < ($ultima_etapa) - 1) {
                $fluxo->etapa_atual ++;
                $json['status'] = 1;
                $json['message'] = 'Atividade atualizada';

                $data['tf_situacao'] = $tf_situacao;
            } else {

                $json['status'] = 1;
                $json['message'] = 'Esta Tarefa foi concluída. O Protocolo de Origem será Atualizado.';
                $json['reload'] = 1;
                // if($result){
                $data['tf_situacao'] = 'Concluída';
                $data['tf_usuario'] = $this->session->userdata('user_id');

                $this->load->model('protocolo_model');

                $tramite['t_descricao'] = 'Concluído';
                $tramite['t_data'] = date('Y-m-d H:m:i');
                $tramite['t_local'] = $this->session->userdata('user_orgao');
                $tramite['t_responsavel'] = $this->session->userdata('user_id');
                $tramite['t_tipo'] = 'Interno';
                $tramite['protocolo_idprotocolo'] = $tarefa->protocolo_idprotocolo;

                $result = $this->protocolo_model->insert_tramitacao($tramite);

                if (($result) and ($idprotocolo != 1)) {

                    $idprotocolo = $tarefa->protocolo_idprotocolo;
                    $p_data['p_status'] = 'Concluído';

                    $this->protocolo_model->update($idprotocolo, $p_data);
                }

                // }
            }

            $data['tf_usuario'] = $this->session->userdata('user_id');
            $data['tf_fluxo'] = json_encode($fluxo);

            $this->tarefa_model->update($id_tarefa, $data);
        }

        echo json_encode($json);
    }

    public function insert_atividade()
    {
        $comentario = $this->input->post('tf_comentario');
        $id_tarefa = $this->input->post('idtarefa');

        $json['status'] = 1;
        $json['message'] = null;

        if (empty($comentario)) {
            $json['status'] = 0;
            $json['error_list']['#tf_comentario'] = 'Preencha o campo comentário';
        }
        ;
        if (empty($id_tarefa)) {
            $json['status'] = 0;
            $json['error_list']['#tf_comentario'] .= 'Tarefa não encontrada.';
        }
        ;

        if ($json['status']) {

            $this->load->model('tarefa_model');

            $tarefa = $this->tarefa_model->get_data($id_tarefa);

            $fluxo = json_decode($tarefa->tf_fluxo);
            $etapa_atual = $fluxo->etapa_atual;

            $atividade = array(
                'at_etapa' => $etapa_atual,
                'at_situacao' => '...',
                'at_descricao' => $comentario,
                'at_data' => date('Y-m-d H:m:i'),
                'tarefa_idtarefa' => $id_tarefa,
                'at_responsavel' => $this->session->userdata('user_name')
            );

            $id_atividade = $this->tarefa_model->insert_atividade($atividade);

            if ($id_atividade) {
                $json['status'] = 1;
                $json['message'] = 'Seu comentário foi incluído na tarefa';
            } else {
                $json['status'] = 0;
                $json['message'] = 'Não foi possível cadastrar o comentário';
            }
        }

        echo json_encode($json);
    }

    public function ajax_criar_tarefa()
    {
        $dados = $this->input->post();

        $json['status'] = 1;

        if (empty($dados['tf_descricao'])) {
            $json['status'] = 0;
            $json['error_list']['#tf_descricao'] = 'Campo descricao está vazio';
        }

        if (empty($dados['tf_fluxo'])) {
            $json['status'] = 0;
            $json['error_list']['#tf_fluxo'] = 'Campo fluxo não selecionado';
        }

        if ($json['status']) {

            $dados['tf_data'] = date('Y-m-d H:m:i');

            // Verifica se um responsável foi atribuido pelo gestor e enviar email de notificação
            if (empty($dados['tf_usuario'])) {
                $dados['tf_usuario'] = $this->session->userdata('user_id');
                $dados['tf_situacao'] = 'Criada';

                $send_mail = false;
            } else {
                $dados['tf_usuario'] = $dados['tf_usuario'];
                $dados['tf_situacao'] = 'Iniciada';

                $send_mail = true;
            }

            switch ($dados['tf_fluxo']) {
                case 1:
                    $fluxo = array(
                        'nome' => 'Receber e Encaminhar',
                        'objetivo' => 'Receber documento e direcionar',
                        'etapa_atual' => 0,
                        'etapas' => array(
                            'Início',
                            'Preparar Memorando',
                            'Encaminhar para Destino'
                        )
                    );
                    break;
                case 2:
                    $fluxo = array(
                        'nome' => 'Receber dar Ciência e Arquivar',
                        'objetivo' => 'Receber documento e arquivar',
                        'etapa_atual' => 0,
                        'etapas' => array(
                            'Início',
                            'Dar Ciência',
                            'Arquivar'
                        )
                    );
                    break;
                case 3:
                    $fluxo = array(
                        'nome' => 'Licitação',
                        'objetivo' => 'Realizar licitação',
                        'etapa_atual' => 0,
                        'etapas' => array(
                            '<h5>Início</h5>',
                            '<h5>Abertura Processo</h5> Solicitação Abertura  <br />Termo Abertura',
                            '<h5>Justificativas</h5> Para contratação  <br />Consonante c/ Planejamento',
                            '<h5>Cotação de Preços</h5> Painel de Preços <br /> Pesquisa Fornecedores Locais',
                            '<h5>Montar Planilha</h5> Apresentação de Preços Médios',
                            '<h5>Anexar Documentos</h5> Solicitação Despesas  <br />Extrato Datação  <br />Termo Compromisso  <br />Termo Referência <br />Leis Municipais  <br />Portaria Secretário',
                            '<h5>Anexar Autorização</h5> Assinatura do Secretário  <br />Assinatura do Prefeito',
                            '<h5>Encaminhar para SEPLAN</h5> Receber Termo de Compromisso',
                            '<h5>Encaminhar para PROGEN</h5> Para Conclusão'
                        )
                    );
                    break;
                default:
                    $fluxo = array(
                        'nome' => 'Receber e Encaminhar',
                        'objetivo' => 'Receber documento e direcionar',
                        'etapa_atual' => 0,
                        'etapas' => array(
                            'Início',
                            'Preparar Memorando',
                            'Encaminhar para Destino'
                        )
                    );
            }

            $dados['tf_fluxo'] = json_encode($fluxo);

            $this->load->model('tarefa_model');

            $id_tarefa = $this->tarefa_model->insert($dados);

            if ($id_tarefa) {

                $atividade = array(
                    'at_etapa' => 0,
                    'at_situacao' => 'Iniciado',
                    'at_descricao' => 'Tarefa Criada',
                    'at_data' => date('Y-m-d H:m:i'),
                    'tarefa_idtarefa' => $id_tarefa,
                    'at_responsavel' => $this->session->userdata('user_name')
                );

                $id_atividade = $this->tarefa_model->insert_atividade($atividade);

                $this->load->model('protocolo_model');

                $tramite['t_descricao'] = 'Documento distribuído';
                $tramite['t_data'] = date('Y-m-d H:m:i');
                $tramite['t_local'] = $this->session->userdata('user_orgao');
                $tramite['t_responsavel'] = $this->session->userdata('user_id');
                $tramite['t_tipo'] = 'Interno';
                $tramite['protocolo_idprotocolo'] = $dados['protocolo_idprotocolo'];

                $this->protocolo_model->insert_tramitacao($tramite);

                // Não atualiza o protocolo que permite criar atividades internas
                if ($dados['protocolo_idprotocolo'] != 1) {
                    $campo['p_status'] = 'Distribuído';
                    $this->protocolo_model->update($dados['protocolo_idprotocolo'], $campo);
                }
            }

            if ($id_tarefa and $id_atividade) {
                $json['status'] = 1;
                $json['message'] = 'Tarefa cadastrada';

                # ----ENVIO EMAIL
                if ($send_mail) {

                    $this->load->model('users_model');
                    $email_destino = $this->users_model->get_data($dados['tf_usuario'], 'u_email');
                    $user['email'] = $email_destino;
                    $user['descricao'] = $dados['tf_descricao'];

                    $dados['mensagem'] = $user;

                    $msg = $this->load->view('mail_new_task', $dados, true);
                    $assunto = 'GPROw - Você tem uma nova tarefa';
                    
                   // var_dump($email_destino);
                    
                    $email = $email_destino->u_email;

                    
                    $this->_send_email($email, $msg, $assunto);
                }

                # ---
            } else {
                $json['message'] = 'Não foi possivel concluir o cadastro';
            }
        }
        echo json_encode($json);
    }

    private function _send_email($email, $msg, $assunto)
    {
        $this->load->library('email');

        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->from('gprow_smsi@paradigmasistemas.com', 'GPROw - Gerenciador de Tarefas');
        $this->email->to($email);

        $this->email->subject($assunto);
        $this->email->message($msg);

        $this->email->send();
    }

    public function ajax_list_tarefas($status = NULL, $filtro = NULL)
    {
        if (! $this->input->is_ajax_request()) {
            // exit('Acesso direto não permitido');
        }
        ;

        if (($filtro == 'usuario') and ($this->session->user_tipo != 'Gestor')) {
            $_POST['usuario'] = $this->session->user_id;
            // echo $situacao;
        }

        if ($status) {
            $_POST['status'] = $status;
            // echo $situacao;
        }
        $this->load->model('tarefa_model');
        $tarefas = $this->tarefa_model->get_dataTable();

        $data = array();

        foreach ($tarefas as $tarefa) {
            $row = array();
            $row[] = date_format(date_create($tarefa->tf_data), 'd/m/y');
            $row[] = $tarefa->tf_descricao;
            $row[] = $tarefa->p_num_origem . '<br> - ' . $tarefa->p_origem;
            // $row[] = $tarefa->tf_situacao;
            $row[] = '<div style="display:inline-block">
                        <button class="btn btn-light btn-view-detalhes" 
                        tarefa_id="' . $tarefa->idtarefa . '">
	                       <i class="fa fa-search-plus">&nbsp;Detalhes</i>
                        </button>
                      </div> 
                    ';

            $data[] = $row;
        }

        $json = array(
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $this->tarefa_model->records_total(),
            'recordsFiltered' => $this->tarefa_model->records_filtered(),
            'data' => $data
        );

        echo json_encode($json);
    }

    public function ajax_list_tramitacao($id)
    {
        if (! $this->input->is_ajax_request()) {
            exit('Acesso direto não permitido');
        }
        ;

        $this->load->model('protocolo_model');
        $tramitacao = $this->protocolo_model->get_tramitacao($id);

        $dados['tramitacao'] = $tramitacao;
        $dados['idprotocolo'] = $id;

        $json['data'] = $this->load->view('list_tramitacao', $dados, true);
        $json['status'] = 1;

        echo json_encode($json);
    }

    public function ajax_get_detalhes_tarefa($idtarefa)
    {
        $json['status'] = 0;

        $this->load->model('tarefa_model');
        // $idtarefa = $this->input->post('id_tarefa');

        $dados['tarefa'] = $this->tarefa_model->get_data($idtarefa);

        if ($dados['tarefa']) {

            $json['status'] = 1;
            // var_dump($dados);
            $dados['atividades'] = $this->tarefa_model->list_atividades($idtarefa);
        }

        $json['data'] = $this->load->view('detalhes_tarefa', $dados, true);

        echo json_encode($json);
    }
}
