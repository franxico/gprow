<p />
<div class="container-fluid">
	<div class="col-lg-12">
		<h4>Tarefa:</h4>
		<div class="alert alert-info col-lg-7">
			<cite><?php echo $tarefa->tf_descricao; ?></cite>
		</div>
		<div class="col-lg-3">
<?php
$fluxo = json_decode($tarefa->tf_fluxo);

if ($tarefa->tf_situacao != 'Concluída') {

    ?>

	<button id="bnt_proxima_etapa" class="btn btn-success"
				tarefa_id="<?php echo $tarefa->idtarefa?>"><?php echo ($tarefa->tf_situacao == 'Criada')?'Assumir Tarefa':'Concluir Etapa'?></button>
			<span class="help-block"></span>

<?php
}
?>	
	</div>


		<div class="col-lg-12">

			<h4>Fluxo de trabalho:</h4>
			<div class="row" style="margin: 30px;"> 

<?php
foreach ($fluxo->etapas as $key => $etapa) {

    ?>
   
    <span
					class="seta_fluxo <?php if($fluxo->etapa_atual==$key){echo 'fluxo_active';};?>"><?php echo $etapa;?></span>
<?php
}
;
?>
</div>



			<h4>Atividades:</h4>	
<?php

foreach ($atividades as $atividade) {

    $nome_etapa = explode('</h5>', $fluxo->etapas[$atividade->at_etapa]);
    $nome_etapa = ltrim($nome_etapa[0],'<h5>');
    
    echo '<div class="divisor"> - ' . date_format(date_create($atividade->at_data), 'd/m/y') . ': <strong>' . $nome_etapa . ' [</strong>' . $atividade->at_descricao . '<strong>] </strong><BR><em>Autor: ' . $atividade->at_responsavel . '<br /></em></div>';

    ?>

<?php };?>	

					<div class="container" style="padding: 10px;">

				<div class="col-lg-8" id="comment_box">

					<form name="form_comment_tf" id="form_comment_tf"
						class="form-inline" action="#" method="post">
						<div>
							<label>Adicionar Comentário:</label>
						</div>
						<div class="form-group">
							<input type="hidden" id="idtarefa" name="idtarefa"
								value="<?php echo $tarefa->idtarefa?>" />
							<textarea id="tf_comentario" name="tf_comentario" rows="2"
								cols="45" class="form-control"></textarea>
							&nbsp;
							<button type="submit" id="btn_save_comment" class="btn">Enviar</button>
							<span class="help-block"></span>

						</div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
<style>

h5{
    font-weight:bold;
}
.divisor {
	border-bottom: 1px solid #d9edf7;
	padding: 5px 0 15px 0;
	width: 70%;
	margin-left: 2%;
}

.seta_fluxo {
	padding: 10px 10px;
	background: #fbf8d0;
	border: 4px solid #fff;
	border-radius: 0 25px 25px 0;
	font-style: italic;
	font-size: 0.9em;
	display: inline-block;
	vertical-align: text-top;
}

.fluxo_active {
	font-weight: 600;
	background: #b0f1af;
	border-bottom: 3px solid #808080;
	font-style: normal;
	font-size: 1em;
}
</style>

<script>

$('#bnt_proxima_etapa').click(function(){
    
	   id_tarefa = $(this).attr('tarefa_id');                
     $.ajax({
         type:'POST',
         url: BASE_URL+'tarefa/avancar_tarefa/'+id_tarefa,
         dataType:'json',
         data:$(this).serialize(),
         beforeSend:function(){
         clearErrors();

         id = '#bnt_proxima_etapa';
         $(id).siblings(".help-block").html(loadingImg('Aguarde...'));

         },
 success:function(response){
     //clearErrors();

     if(response['status']){

         //$("#list-tarefa").html(response['data']);

         window.alert(response['message']);
                    
         $('#modal_detalhes').modal('hide');

         
     }else{
         showErrorsModal(response['error_list']);
     }
 },
     error: function(response){
          console.log(response);
         }
         
     });                

 });


$('#form_comment_tf').submit(function(){


	console.log($(this).serialize());
    $.ajax({
        type:'POST',
        url: BASE_URL+'tarefa/insert_atividade',
        dataType:'json',
        data:$(this).serialize(),
        beforeSend:function(){
        clearErrors();
                        
            id = '#btn_save_comment';
            $(id).siblings(".help-block").html(loadingImg('Aguarde...'));

        },
success:function(response){
    clearErrors();

    if(response['status']){
        window.alert(response['message']);
        $('#modal_detalhes').modal('hide');
        
    }else{
        showErrorsModal(response['error_list']);
    }
},
    error: function(response){
         console.log(response);
        }
        
    });

    
   return false;
});


</script>